package zumra.classifier;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NaiveBayesClassifierTest {
    private final NaiveBayesClassifier<String> classifier = new NaiveBayesClassifier<>(Classifier::getfeatures);

    @Before
    public void sampleTrain() {
        classifier.train("Nobody owns the water.", "good");
        classifier.train("the quick rabbit jumps fences", "good");
        classifier.train("buy pharmaceuticals now", "bad");
        classifier.train("make quick money at the online casino", "bad");
        classifier.train("the quick brown fox jumps", "good");
    }

    @Test
    public void quickRabbitIsMoreGoodThanBad() {
        assertEquals(0.15625, classifier.prob("quick rabbit", "good"), 0.00001);
        assertEquals(0.05, classifier.prob("quick rabbit", "bad"), 0.001);
    }

    @Test
    public void classifyNewDocument() {
        assertEquals("good", classifier.classify("quick rabbit", "unknown").getA());
        assertEquals("bad", classifier.classify("quick money", "unknown").getA());
        classifier.setthreshold("bad", 3.0);
        assertEquals("unknown", classifier.classify("quick money", "unknown").getA());
        for (int i = 0; i < 10; i++) {
            sampleTrain();
        }
        assertEquals("bad", classifier.classify("quick money", "unknown").getA());
    }
}
