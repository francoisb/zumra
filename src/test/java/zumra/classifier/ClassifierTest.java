package zumra.classifier;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClassifierTest {
    public class MyClassifier extends Classifier<String> {
        @Override
        public double prob(String item, String cat) {
            return 0;
        }

        public MyClassifier(Function<String, Set<String>> getfeatures) {
            super(getfeatures);
        }
    }

    private final Classifier<String> classifier = new MyClassifier(Classifier::getfeatures);

    @Before
    public void sampleTrain() {
        classifier.train("Nobody owns the water.", "good");
        classifier.train("the quick rabbit jumps fences", "good");
        classifier.train("buy pharmaceuticals now", "bad");
        classifier.train("make quick money at the online casino", "bad");
        classifier.train("the quick brown fox jumps", "good");
    }

    @Test
    public void incrementsCategories() {
        assertTrue(classifier.categories().size() == 2);
    }

    @Test
    public void incrementsCategoryCount(){
        assertEquals(3, classifier.catcount("good").intValue());
        assertEquals(2, classifier.catcount("bad").intValue());
    }

    @Test
    public void incrementsFeaturesWithExistentFeaturesInKnownCategories() {
        assertEquals(2, classifier.fcount("quick", "good").intValue());
        assertEquals(1, classifier.fcount("quick", "bad").intValue());
    }

    @Test
    public void incrementsFeaturesWithUnknownFeatureInKnownCategory() {
        assertEquals(0, classifier.fcount("junior", "good").intValue());
        assertEquals(0, classifier.fcount("junior", "bad").intValue());
    }

    @Test
    public void incrementsFeaturesWithKnownFeatureInUnnownCategory() {
        assertEquals(0, classifier.fcount("quick", "what category?").intValue());
    }

    @Test
    public void incrementsFeaturesWithUnknownFeatureInUnnownCategory() {
        assertEquals(0, classifier.fcount("target", "what category?").intValue());
    }

    @Test
    public void calculatesFeatureProbability() {
        assertEquals(0.66667, classifier.fprob("quick", "good"), 0.0001);
    }

    @Test
    public void calculatesWeightedProbability() {
        assertEquals(0.25, classifier.weightedprob("money", "good", (t2) -> classifier.fprob(t2.getA(), t2.getB()), 1.0, 0.5), 0.00001);
        sampleTrain();
        assertEquals(0.16667, classifier.weightedprob("money", "good", (t2) -> classifier.fprob(t2.getA(), t2.getB()), 1.0, 0.5), 0.00001);
    }

    @Test
    public void calculatesTotalCount() {
        assertEquals(5, classifier.totalcount().intValue());
        classifier.train("The authors of this paper wish to thank...", "research paper");
        assertEquals(6, classifier.totalcount().intValue());
    }
}
