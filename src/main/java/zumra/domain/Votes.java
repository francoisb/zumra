package zumra.domain;

import com.sun.istack.NotNull;

import java.util.Set;

public class Votes {
    @NotNull
    private final Set<Integer> ids;

    public Votes(Set<Integer> ids) {
        if (ids == null) throw new IllegalArgumentException("Must only pass non-null ids collection");
        this.ids = ids;
    }

    public Set<Integer> getIds() {
        return ids;
    }
}
