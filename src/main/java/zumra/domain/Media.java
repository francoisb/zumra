package zumra.domain;

public enum Media {
    TWITTER("Twitter"), FACEBOOK("Facebook"), HACKER_NEWS("Hacker News"), GOOGLE_PLUS("Google+");

    private final String name;

    Media(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return this.name;
    }

    @Override
    public String toString() {
        return name;
    }
}
