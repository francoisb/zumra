package zumra.domain;

import com.sun.istack.NotNull;
import org.joda.time.DateTime;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Recommendation {
    private final int id;
    @NotNull
    private final URI url;
    @NotNull
    private final String hostname;
    private final double weight;
    @NotNull
    private final Set<String> bodies;
    @NotNull
    private final DateTime harvestedAt;

    public Recommendation(Integer id, URI url, String hostname, Double weight, String bodies, DateTime harvestedAt) {
        if (url == null) throw new IllegalArgumentException("url must not be null");
        if (hostname == null) throw new IllegalArgumentException("hostname must not be null");
        if (bodies == null) throw new IllegalArgumentException("bodies must not be null");
        if (harvestedAt == null) throw new IllegalArgumentException("harvestedAt must not be null");

        final Set<String> coll = new HashSet<>(5);
        final String[] elements = bodies.split(";––;");
        Collections.addAll(coll, elements);

        this.id = id;
        this.url = url;
        this.hostname = hostname;
        this.weight = weight;
        this.bodies = coll;
        this.harvestedAt = harvestedAt;
    }

    public int getId() {
        return id;
    }

    public URI getUrl() {
        return url;
    }

    public String getHostname() {
        return hostname;
    }

    public Set<String> getBodies() {
        return bodies;
    }

    public DateTime getHarvestedAt() {
        return harvestedAt;
    }

    @Override
    public String toString() {
        return "Recommendation{" +
                "id=" + id +
                ", hostname='" + hostname + '\'' +
                ", bodies=" + bodies.iterator().next() +
                '}';
    }
}
