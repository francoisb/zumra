package zumra.domain;

import org.joda.time.DateTime;

import java.net.URI;

public final class Link {
    private final URI url;
    private final Hostname hostname;
    private final Media service;
    private final String body;
    private final ItemId serviceId;
    private final UserId userId;
    private final ScreenName screeName;
    private final DateTime createdAt;
    private final DateTime harvestedAt;

    public Link(URI url, Hostname hostname, Media service, String body, ItemId serviceId, UserId userId, ScreenName screeName, DateTime createdAt, DateTime harvestedAt) {
        this.url = url;
        this.hostname = hostname;
        this.service = service;
        this.body = body;
        this.serviceId = serviceId;
        this.userId = userId;
        this.screeName = screeName;
        this.createdAt = createdAt;
        this.harvestedAt = harvestedAt;
    }

    public URI getUrl() {
        return url;
    }

    public Hostname getHostname() {
        return hostname;
    }

    public Media getService() {
        return service;
    }

    public String getBody() {
        return body;
    }

    public ItemId getServiceId() {
        return serviceId;
    }

    public UserId getUserId() {
        return userId;
    }

    public ScreenName getScreenName() {
        return screeName;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public DateTime getHarvestedAt() {
        return harvestedAt;
    }

    public boolean isValidServiceId() {
        return serviceId.getId() > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Link link = (Link) o;

        if (!body.equals(link.body)) return false;
        if (!createdAt.equals(link.createdAt)) return false;
        if (!harvestedAt.equals(link.harvestedAt)) return false;
        if (!hostname.equals(link.hostname)) return false;
        if (service != link.service) return false;
        if (!serviceId.equals(link.serviceId)) return false;
        if (!url.equals(link.url)) return false;
        if (!userId.equals(link.userId)) return false;
        if (!screeName.equals(link.screeName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + hostname.hashCode();
        result = 31 * result + service.hashCode();
        result = 31 * result + body.hashCode();
        result = 31 * result + serviceId.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + screeName.hashCode();
        result = 31 * result + createdAt.hashCode();
        result = 31 * result + harvestedAt.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Link{" +
                "url=" + url +
                ", service=" + service +
                ", body='" + body + '\'' +
                '}';
    }
}
