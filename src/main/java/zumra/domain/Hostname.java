package zumra.domain;

public final class Hostname {
    private final String hostname;

    public Hostname(String hostname) {
        if (hostname == null) throw new NullPointerException("Hostname cannot be null");
        this.hostname = hostname;
    }

    public String getHostname() {
        return hostname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hostname hostname1 = (Hostname) o;

        if (!hostname.equals(hostname1.hostname)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return hostname.hashCode();
    }

    @Override
    public String toString() {
        return "Hostname{" +
                "hostname='" + hostname + '\'' +
                '}';
    }
}
