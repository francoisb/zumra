package zumra.classifier;

import java.util.Set;
import java.util.function.Function;

/**
 * An implementation of the Naive Bayes classifier algorithm.
 *
 * <p>Based on the Python code found in <a href="http://shop.oreilly.com/product/9780596529321.do">Programming Collective
 * Intelligence</a>, by Toby Segaran.</p>
 */
public class NaiveBayesClassifier<DOC> extends Classifier<DOC> {
    public NaiveBayesClassifier(Function<DOC, Set<String>> getfeatures) {
        super(getfeatures);
    }

    protected double prob(DOC item, String cat) {
        double catprob = catcount(cat).doubleValue() / totalcount();
        double docprob = docprob(item, cat);
        return docprob * catprob;
    }

    protected double docprob(DOC item, String cat) {
        Set<String> features = getfeatures.apply(item);
        double p = 1;
        for (String f : features) {
            p *= weightedprob(f, cat, (t2) -> this.fprob(t2.getA(), t2.getB()), 1.0, 0.5);
        }

        return p;
    }
}
