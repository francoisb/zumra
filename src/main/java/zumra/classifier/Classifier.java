package zumra.classifier;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @param <DOC> The class of object that can be classified. The getfeatures function will receive this parameter type and extract features from it.
 */
abstract public class Classifier<DOC> {
    private final Map<String, Map<String, Integer>> fc = new HashMap<>();
    private final Map<String, Integer> cc = new HashMap<>();
    private final Map<String, Double> thresholds = new HashMap<>();
    protected final Function<DOC, Set<String>> getfeatures;

    protected Classifier(Function<DOC, Set<String>> getfeatures) {
        this.getfeatures = getfeatures;
    }

    /**
     * Returns the probability that the item is in the category.
     *
     * @param item The item to classify.
     * @param cat  The category in which we want to test.
     * @return A value from 0.0 to 1.0, where 1.0 indicates a very strong probability that the item is in the category.
     */
    abstract protected double prob(DOC item, String cat);

    public double getthreshold(String cat) {
        if (thresholds.get(cat) == null) {
            return 1.0;
        } else {
            return thresholds.get(cat);
        }
    }

    public Map<String, String> statistics() {
        final Map<String, String> results = new LinkedHashMap<>();
        results.put("categoriesCount", String.valueOf(cc.size()));
        results.put("featuresCount", String.valueOf(fc.size()));

        categories().stream().forEach((cat) -> {
            Iterator<Tuple2<String, Integer>> iter = fc.entrySet().stream().map((entry) -> {
                final String feature = entry.getKey();
                final Integer count = entry.getValue().get(cat);
                return new Tuple2<>(feature, count == null ? 0 : count);
            }).sorted((o1, o2) -> {
                if (o1.getB() > o2.getB()) {
                    return -1;
                } else if (o1.getB() < o2.getB()) {
                    return 1;
                } else {
                    return 0;
                }
            }).limit(10).iterator();

            while (iter.hasNext()) {
                final Tuple2<String, Integer> item = iter.next();
                results.put(format("%s:%s", cat, item.getA()), item.getB().toString());
            }
        });

        return results;
    }

    public Tuple2<String, Double> classify(DOC item, String defcat) {
        final Map<String, Double> probs = new HashMap<>(categories().size());
        double max = 0.0;
        String best = null;
        for (String cat : categories()) {
            probs.put(cat, prob(item, cat));
            if (probs.get(cat) > max) {
                max = probs.get(cat);
                best = cat;
            }
        }

        for (String cat : categories()) {
            if (!cat.equals(best)) {
                if (probs.get(cat) * getthreshold(best) > probs.get(best)) {
                    return new Tuple2<>(defcat, 0.0);
                }
            }
        }

        return new Tuple2<>(best, probs.get(best));
    }

    public void setthreshold(String cat, double threshold) {
        thresholds.put(cat, threshold);
    }

    protected void incf(String f, String cat) {
        if (fc.get(f) == null) {
            fc.put(f, new HashMap<>());
        }

        final Map<String, Integer> feature = fc.get(f);
        final Integer currentCount = feature.get(cat);
        if (currentCount == null) {
            feature.put(cat, 1);
        } else {
            feature.put(cat, 1 + currentCount);
        }
    }

    protected void incc(String cat) {
        if (cc.get(cat) == null) {
            cc.put(cat, 1);
        } else {
            cc.put(cat, 1 + cc.get(cat));
        }
    }

    protected Integer fcount(String f, String cat) {
        if (fc.get(f) == null) {
            return 0;
        } else {
            if (fc.get(f).get(cat) == null) {
                return 0;
            } else {
                return fc.get(f).get(cat);
            }
        }
    }

    protected Integer catcount(String cat) {
        if (cc.get(cat) == null) {
            return 0;
        } else {
            return cc.get(cat);
        }
    }

    protected Integer totalcount() {
        int total = 0;
        for (Integer n : cc.values()) {
            total = total + n;
        }

        return total;
    }

    protected double fprob(String f, String cat) {
        final Integer catcount = catcount(cat);
        if (catcount == 0) {
            return 0.0;
        } else {
            final Integer fcount = fcount(f, cat);
            return 1.0 * fcount / catcount;
        }
    }

    /**
     * @param f      feature
     * @param cat    category
     * @param weight weight of the assumed probability
     * @param ap     assumed probability
     * @return The weighted probability of this feature being in this category.
     */
    protected double weightedprob(String f, String cat, Function<Tuple2<String, String>, Double> prf, double weight, double ap) {
        double basicprob = prf.apply(new Tuple2<>(f, cat));
        int totals = 0;
        for (String c : categories()) {
            totals += fcount(f, c);
        }

        return ((weight * ap) + (totals * basicprob)) / (weight + totals);
    }

    protected Set<String> categories() {
        return cc.keySet();
    }

    public void train(DOC item, String cat) {
        final Set<String> features = getfeatures.apply(item);
        for (String feature : features) {
            incf(feature, cat);
        }

        incc(cat);
    }

    public static Set<String> getfeatures(String s) {
        final String[] arr = s.toLowerCase().split("\\W+");
        final Set<String> features = new HashSet<>();
        Collections.addAll(features, arr);
        return features.stream().filter((n) -> n.length() > 2 && n.length() < 20).collect(Collectors.toSet());
    }

    public static final Set<String> stopWords = buildStopWordsList();

    private static Set<String> buildStopWordsList() {
        final Set<String> words = new HashSet<>();
        Collections.addAll(words,
                "a", "about", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes",
                "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "computer", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else",
                "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here",
                "hereafter", "hereby", "herein", "hereupon", "hers", "herse", "him", "himse", "his", "how", "however", "hundred", "i", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itse", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine",
                "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myse", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves",
                "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than",
                "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon",
                "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves");
        return words;
    }
}
