package zumra;

import com.twitter.Regex;
import zumra.classifier.Classifier;
import zumra.classifier.NaiveBayesClassifier;
import zumra.classifier.Tuple2;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class BingoClass {
    private static final Logger LOG = LoggerFactory.getLogger(BingoClass.class);
    private static final String GET_CLASSIFIED_DOCUMENTS = "SELECT id, trim(url) url, hostname, service, screen_name, body, vote FROM links JOIN link_sources USING (url) WHERE vote IS NOT NULL";
    private static final String GET_UNCLASSIFIED_DOCUMENTS = "SELECT id, trim(url) url, hostname, service, screen_name, body, vote FROM links JOIN link_sources USING (url) WHERE vote IS NULL";

    public static void main(String[] args) throws ClassNotFoundException, SQLException, URISyntaxException {
        Class.forName("org.postgresql.Driver");
        final DataSource dataSource = setupDataSource("jdbc:postgresql://localhost:5433/hnapi", "hnapi", "hnapi");
        final Set<Doc> docs = new HashSet<Doc>();

        try (Connection conn = dataSource.getConnection()) {
            try (Statement stat = conn.createStatement()) {
                try (ResultSet rs = stat.executeQuery(GET_CLASSIFIED_DOCUMENTS)) {
                    while (rs.next()) {
                        final Doc doc = new Doc();
                        doc.id = rs.getLong("id");
                        doc.uri = new URI(rs.getString("url"));
                        doc.hostname = rs.getString("hostname");
                        doc.service = rs.getString("service");
                        doc.screenName = rs.getString("screen_name");
                        doc.body = rs.getString("body");
                        doc.vote = rs.getBoolean("vote");
                        docs.add(doc);
                    }
                }
            }
        }

        LOG.info("Number of documents: {}", docs.size());
        final long startAt = System.currentTimeMillis();
        Classifier<Doc> classifier = new NaiveBayesClassifier<>(BingoClass::getfeatures);
        classifier.setthreshold("good", 3);
        for (Doc d : docs) {
            classifier.train(d, d.vote ? "good" : "bad");
        }
        final long endAt = System.currentTimeMillis();

        LOG.info("Classifier trained in {}ms", endAt - startAt);
        final Map<String, String> stats = classifier.statistics();
        for (Map.Entry<String, String> entry : stats.entrySet()) {
            LOG.info("{} -> {}", entry.getKey(), entry.getValue());
        }

        docs.clear();

        try (Connection conn = dataSource.getConnection()) {
            try (Statement stat = conn.createStatement()) {
                try (ResultSet rs = stat.executeQuery(GET_UNCLASSIFIED_DOCUMENTS)) {
                    while (rs.next()) {
                        final Doc doc = new Doc();
                        doc.id = rs.getLong("id");
                        doc.uri = new URI(rs.getString("url"));
                        doc.hostname = rs.getString("hostname");
                        doc.service = rs.getString("service");
                        doc.screenName = rs.getString("screen_name");
                        doc.body = rs.getString("body");
                        doc.vote = rs.getBoolean("vote");
                        docs.add(doc);
                    }
                }
            }
        }

        LOG.info("===");
        LOG.info("BAD DOCUMENTS");
        logDocsInCategory(docs, classifier, "bad", 15);
        LOG.info("---");
        LOG.info("GOOD DOCUMENTS");
        logDocsInCategory(docs, classifier, "good", 15);
        LOG.info("===");
    }

    private static void logDocsInCategory(Set<Doc> docs, Classifier<Doc> classifier, String wantedCategory, int count) {
        docs.stream().map((doc) -> {
            final Tuple2<String, Double> classification = classifier.classify(doc, "unknown");
            return new Tuple2<>(doc, classification);
        }).filter((t2) -> t2.getB().getA().equals(wantedCategory)).sorted((o1, o2) -> {
            if (o1.getB().getB() > o2.getB().getB()) {
                return 1;
            } else if (o1.getB().getB() < o2.getB().getB()) {
                return -1;
            } else {
                return 0;
            }
        }).limit(count).forEach((result) -> {
            final Doc doc = result.getA();
            final Double score = result.getB().getB();
            LOG.info("({}) @{}: {}", score, doc.screenName, doc.body);
            LOG.info("{}", getfeatures(doc));
        });
    }

    public static Set<String> getfeatures(Doc d) {
        final HashSet<String> results = new HashSet<>();
        results.addAll(getFeaturesWithBigramsNoStopWords(d.body));
        if (d.body.matches("^RT")) results.add("RETWEET");
        results.add("author:" + d.screenName.toLowerCase());
        results.add("hostname:" + d.hostname);
        // results.add("service:" + d.service);

        return results;
    }

    public static Set<String> getFeaturesWithBigramsNoStopWords(String s) {
        Matcher m = Regex.VALID_URL.matcher(s.toLowerCase());
        final String noUrls = m.replaceAll("");
        final String[] arr = noUrls.split("\\W+");
        final List<String> features = new ArrayList<>(arr.length);
        Collections.addAll(features, arr);
        features.removeAll(Classifier.stopWords);
        for (int i = features.size() - 2; i >= 0; i--) {
            features.add(features.get(i) + " " + features.get(i + 1));
        }
        return features.stream().filter((n) -> n.length() > 2 && n.length() < 20).collect(Collectors.toSet());
    }

    private static DataSource setupDataSource(String connectURI, String username, String password) {
        LOG.info("Connecting to {} using {}/{}", connectURI, username, "*****");

        DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, username, password);
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
        ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
        poolableConnectionFactory.setPool(connectionPool);
        return new PoolingDataSource<>(connectionPool);
    }
}
