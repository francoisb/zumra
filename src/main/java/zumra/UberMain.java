package zumra;

import zumra.camel.BootTwitterConsumersOnceContextStarts;
import zumra.harvesters.TwitterBackfillHarvester;
import zumra.harvesters.TwitterOAuthConfiguration;
import zumra.harvesters.TwitterStreamingHarvester;
import zumra.harvesters.TwitterUserStreamProducer;
import zumra.mappers.LinkMapper;
import zumra.mappers.Version;
import zumra.routes.ClientUI;
import zumra.routes.DatabaseAppenderPipeline;
import zumra.routes.HackerNewsProcessingPipeline;
import zumra.routes.TwitterProcessingPipeline;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mybatis.MyBatisComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import static java.util.concurrent.TimeUnit.SECONDS;

public class UberMain {
    private static final Logger LOG = LoggerFactory.getLogger(UberMain.class);

    public static void main(final String[] args) throws Exception {
        final Properties props = loadSystemPreferences();
        final CamelContext context = new DefaultCamelContext();

        final TwitterOAuthConfiguration oauth = new TwitterOAuthConfiguration();
        oauth.consumerKey = props.getProperty("twitter.consumerKey");
        oauth.consumerSecret = props.getProperty("twitter.consumerSecret");
        oauth.accessToken = props.getProperty("twitter.accessToken");
        oauth.accessTokenSecret = props.getProperty("twitter.accessTokenSecret");

        Class.forName("org.postgresql.Driver");
        final String jdbcConnectionUrl = props.getProperty("db.url");
        final String username = props.getProperty("db.username");
        final String password = props.getProperty("db.password");
        final DataSource dataSource = setupDataSource(jdbcConnectionUrl, username, password);
        final SqlSessionFactory sqlSessionFactory = buildSessionFactory(dataSource);
        try (SqlSession session = sqlSessionFactory.openSession(true)) {
            LinkMapper mapper = session.getMapper(LinkMapper.class);
            Version version = mapper.getDatabaseVersion();
            LOG.info("DB Version (MyBatis): {}", version.getVersion());
        }

        final String initialTwitterEndpoint = "direct:twitter-interaction-received";
        final String databaseAppenderEndpoint = "direct:database-appender-pipeline";

        final ProducerTemplate streamProducer = context.createProducerTemplate();
        streamProducer.setDefaultEndpointUri(initialTwitterEndpoint);
        final TwitterUserStreamProducer statusListener = new TwitterUserStreamProducer(streamProducer);
        final TwitterStreamingHarvester streamingHarvester = new TwitterStreamingHarvester(oauth, statusListener);

        final ProducerTemplate backfillProducer = context.createProducerTemplate();
        backfillProducer.setDefaultEndpointUri(initialTwitterEndpoint);
        final TwitterBackfillHarvester backfillHarvester = new TwitterBackfillHarvester(oauth, backfillProducer);

        if (Boolean.valueOf(props.getProperty("twitter.streamEnabled"))) {
            context.addStartupListener(new BootTwitterConsumersOnceContextStarts(streamingHarvester));
        }

        context.addRoutes(new TwitterProcessingPipeline(initialTwitterEndpoint, databaseAppenderEndpoint, backfillHarvester));
        context.addRoutes(new HackerNewsProcessingPipeline(databaseAppenderEndpoint));
        context.addRoutes(new DatabaseAppenderPipeline(databaseAppenderEndpoint, sqlSessionFactory));

        final String uiHost = props.getProperty("ui.host");
        final Integer uiPort = Integer.valueOf(props.getProperty("ui.port"));
        final Boolean uiEnableTemplateCaching = Boolean.valueOf(props.getProperty("ui.enableTemplateCaching"));
        context.addRoutes(new ClientUI(uiHost, uiPort, uiEnableTemplateCaching));

        final MyBatisComponent myBatisComponent = new MyBatisComponent();
        myBatisComponent.setSqlSessionFactory(sqlSessionFactory);
        context.addComponent("mybatis", myBatisComponent);

        context.start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                LOG.info("Shutting down...");
                streamingHarvester.close();
                try {
                    context.stop();
                    new CountDownLatch(1).await(1, SECONDS);
                } catch (Exception e) {
                    LOG.warn("Received exception while shutting down", e);
                } finally {
                    LOG.info("Exiting!");
                }
            }
        });
    }

    private static Properties loadSystemPreferences() throws URISyntaxException {
        Properties props = new Properties();

        setPropertyOrRaiseIfAbsent("twitter.streamEnabled", "TWITTER_STREAM_ENABLED", props);
        setPropertyOrRaiseIfAbsent("twitter.consumerKey", "TWITTER_CONSUMER_KEY", props);
        setPropertyOrRaiseIfAbsent("twitter.consumerSecret", "TWITTER_CONSUMER_SECRET", props);
        setPropertyOrRaiseIfAbsent("twitter.accessToken", "TWITTER_ACCESS_TOKEN", props);
        setPropertyOrRaiseIfAbsent("twitter.accessTokenSecret", "TWITTER_ACCESS_TOKEN_SECRET", props);

        setPropertyOrRaiseIfAbsent("db.url", "DATABASE_URL", props);
        final URI url = new URI(props.getProperty("db.url").replaceFirst("^jdbc:", ""));
        final String userInfo = url.getUserInfo();
        final String[] credentials = userInfo.split(":");
        props.setProperty("db.url", "jdbc:" + url.toString().replaceFirst(userInfo + "@", "").replaceFirst("postgres\\b", "postgresql"));
        props.setProperty("db.username", credentials[0]);
        props.setProperty("db.password", credentials[1]);

        setPropertyOrRaiseIfAbsent("ui.port", "PORT", props);
        props.setProperty("ui.host", "0.0.0.0");
        props.setProperty("ui.enableTemplateCaching", "false");

        return props;
    }

    private static void setPropertyOrRaiseIfAbsent(String propKey, String key, Properties props) {
        final String value = System.getenv(key);
        if (value == null) throw new IllegalArgumentException("Key " + key + " is absent from environment");
        props.setProperty(propKey, value);
    }

    private static SqlSessionFactory buildSessionFactory(DataSource dataSource) {
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("development", transactionFactory, dataSource);
        Configuration configuration = new Configuration(environment);
        configuration.addMappers("info.teksol.mappers");
        configuration.getTypeHandlerRegistry().register("info.teksol.mappers");

        return new SqlSessionFactoryBuilder().build(configuration);
    }

    private static DataSource setupDataSource(String connectURI, String username, String password) {
        LOG.info("Connecting to {} using {}/{}", connectURI, username, "*****");

        DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, username, password);
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
        ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
        poolableConnectionFactory.setPool(connectionPool);
        return new PoolingDataSource<>(connectionPool);
    }
}
