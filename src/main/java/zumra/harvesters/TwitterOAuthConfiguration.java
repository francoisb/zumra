package zumra.harvesters;

public class TwitterOAuthConfiguration {
    public String consumerKey;
    public String consumerSecret;
    public String accessToken;
    public String accessTokenSecret;
}
