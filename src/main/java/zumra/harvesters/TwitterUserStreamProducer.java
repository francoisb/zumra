package zumra.harvesters;

import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.UserStreamAdapter;

public class TwitterUserStreamProducer extends UserStreamAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(TwitterUserStreamProducer.class);

    private final ProducerTemplate statusProducer;

    public TwitterUserStreamProducer(ProducerTemplate statusProducer) {
        this.statusProducer = statusProducer;
    }

    @Override
    public void onStatus(Status status) {
        LOG.trace("{}", status);
        statusProducer.sendBody(status);
    }
}
