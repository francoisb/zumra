package zumra.harvesters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.ConnectionLifeCycleListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.UserStreamListener;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterStreamingHarvester implements ConnectionLifeCycleListener {
    private static final Logger LOG = LoggerFactory.getLogger(TwitterStreamingHarvester.class);
    private final TwitterStream stream;

    public TwitterStreamingHarvester(TwitterOAuthConfiguration oauth, UserStreamListener listener) {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(oauth.consumerKey);
        cb.setOAuthConsumerSecret(oauth.consumerSecret);
        cb.setOAuthAccessToken(oauth.accessToken);
        cb.setOAuthAccessTokenSecret(oauth.accessTokenSecret);

        this.stream = new TwitterStreamFactory(cb.build()).getInstance();
        stream.addListener(listener);
        stream.addConnectionLifeCycleListener(this);
    }

    @Override
    public void onConnect() {
        LOG.info("Connected to Twitter");
    }

    @Override
    public void onDisconnect() {
        LOG.info("Disconnected from Twitter");
    }

    @Override
    public void onCleanUp() {
        LOG.info("Cleaning up Twitter");
    }

    public void start() {
        stream.user();
    }

    public void close() {
        stream.clearListeners();
        stream.cleanUp();
        stream.shutdown();
    }
}
