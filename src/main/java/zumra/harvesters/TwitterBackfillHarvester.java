package zumra.harvesters;

import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterBackfillHarvester {
    private static final Logger LOG = LoggerFactory.getLogger(TwitterBackfillHarvester.class);
    private final ProducerTemplate producerTemplate;
    private final Twitter twitter;
    private final int maxBackfillCount;

    public TwitterBackfillHarvester(TwitterOAuthConfiguration oauth, ProducerTemplate producerTemplate) {
        this(oauth, producerTemplate, 200);
    }

    public TwitterBackfillHarvester(TwitterOAuthConfiguration oauth, ProducerTemplate producerTemplate, int maxBackfillCount) {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(oauth.consumerKey);
        cb.setOAuthConsumerSecret(oauth.consumerSecret);
        cb.setOAuthAccessToken(oauth.accessToken);
        cb.setOAuthAccessTokenSecret(oauth.accessTokenSecret);

        this.twitter = new TwitterFactory(cb.build()).getInstance();
        this.producerTemplate = producerTemplate;
        this.maxBackfillCount = maxBackfillCount;
    }

    /**
     * Reads the home timeline of the authenticated user and backfills at most <code>maxBackfillCount</code> items.
     */
    public void backfill() {
        LOG.info("Backfilling Twitter home timeline: want {} tweets max", maxBackfillCount);
        Paging page = new Paging();
        page.setCount(maxBackfillCount);
        try {
            ResponseList<Status> statuses = twitter.getHomeTimeline(page);
            statuses.forEach(producerTemplate::sendBody);

            LOG.info("Backfill completed; found {} tweets", statuses.size());
        } catch (TwitterException e) {
            LOG.error("Failed to backfill home timeline", e);
            LOG.warn("Ignoring failed request to backfill home timeline");
        }
    }
}
