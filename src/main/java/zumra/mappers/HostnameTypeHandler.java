package zumra.mappers;

import zumra.domain.Hostname;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

@MappedTypes(Hostname.class)
public class HostnameTypeHandler implements TypeHandler<Hostname> {
    @Override
    public void setParameter(PreparedStatement ps, int i, Hostname parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.VARCHAR);
        } else {
            ps.setString(i, parameter.getHostname());
        }
    }

    @Override
    public Hostname getResult(ResultSet rs, String columnName) throws SQLException {
        return new Hostname(rs.getString(columnName));
    }

    @Override
    public Hostname getResult(ResultSet rs, int columnIndex) throws SQLException {
        return new Hostname(rs.getString(columnIndex));
    }

    @Override
    public Hostname getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return new Hostname(cs.getString(columnIndex));
    }
}
