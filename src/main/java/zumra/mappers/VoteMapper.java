package zumra.mappers;

import zumra.domain.Recommendation;
import zumra.domain.Votes;

import java.net.URI;
import java.util.List;
import java.util.Set;

public interface VoteMapper {
    public Set<URI> discardLinks(Votes votes);

    public URI findUrlById(Votes votes);

    public List<Recommendation> findBestRecommendations();
}
