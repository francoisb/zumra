package zumra.mappers;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

@MappedTypes(URI.class)
public class URITypeHandler implements TypeHandler<URI> {
    @Override
    public void setParameter(PreparedStatement ps, int i, URI parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.OTHER);
        } else {
            ps.setObject(i, parameter, Types.OTHER);
        }
    }

    @Override
    public URI getResult(ResultSet rs, String columnName) throws SQLException {
        try {
            return new URI(rs.getString(columnName));
        } catch (URISyntaxException e) {
            throw new SQLException("Failed to turn \"" + rs.getString(columnName) + "\" into a URI object", e);
        }
    }

    @Override
    public URI getResult(ResultSet rs, int columnIndex) throws SQLException {
        try {
            return new URI(rs.getString(columnIndex));
        } catch (URISyntaxException e) {
            throw new SQLException("Failed to turn \"" + rs.getString(columnIndex) + "\" into a URI object", e);
        }
    }

    @Override
    public URI getResult(CallableStatement cs, int columnIndex) throws SQLException {
        try {
            return new URI(cs.getString(columnIndex));
        } catch (URISyntaxException e) {
            throw new SQLException("Failed to turn \"" + cs.getString(columnIndex) + "\" into a URI object", e);
        }
    }
}
