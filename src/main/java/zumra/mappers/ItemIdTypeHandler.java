package zumra.mappers;

import zumra.domain.ItemId;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

@MappedTypes(ItemId.class)
public class ItemIdTypeHandler implements TypeHandler<ItemId> {
    @Override
    public void setParameter(PreparedStatement ps, int i, ItemId parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.BIGINT);
        } else {
            ps.setLong(i, parameter.getId());
        }
    }

    @Override
    public ItemId getResult(ResultSet rs, String columnName) throws SQLException {
        return new ItemId(rs.getLong(columnName));
    }

    @Override
    public ItemId getResult(ResultSet rs, int columnIndex) throws SQLException {
        return new ItemId(rs.getLong(columnIndex));
    }

    @Override
    public ItemId getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return new ItemId(cs.getLong(columnIndex));
    }
}
