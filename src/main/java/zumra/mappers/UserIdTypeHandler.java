package zumra.mappers;

import zumra.domain.UserId;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

@MappedTypes(UserId.class)
public class UserIdTypeHandler implements TypeHandler<UserId> {
    @Override
    public void setParameter(PreparedStatement ps, int i, UserId parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.BIGINT);
        } else {
            ps.setLong(i, parameter.getId());
        }
    }

    @Override
    public UserId getResult(ResultSet rs, String columnName) throws SQLException {
        return new UserId(rs.getLong(columnName));
    }

    @Override
    public UserId getResult(ResultSet rs, int columnIndex) throws SQLException {
        return new UserId(rs.getLong(columnIndex));
    }

    @Override
    public UserId getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return new UserId(cs.getLong(columnIndex));
    }
}
