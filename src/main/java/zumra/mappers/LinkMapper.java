package zumra.mappers;

import zumra.domain.Link;

import java.util.Collection;

public interface LinkMapper {
    public Version getDatabaseVersion();

    public void insertLinks(Collection<Link> links);
}
