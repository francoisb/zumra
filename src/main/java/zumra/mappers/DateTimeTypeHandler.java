package zumra.mappers;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.joda.time.DateTime;

import java.sql.*;

@MappedTypes(DateTime.class)
public class DateTimeTypeHandler implements TypeHandler<DateTime> {
    @Override
    public void setParameter(PreparedStatement ps, int i, DateTime parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.DATE);
        } else {
            ps.setTimestamp(i, new Timestamp(parameter.getMillis()));
        }
    }

    @Override
    public DateTime getResult(ResultSet rs, String columnName) throws SQLException {
        return new DateTime(rs.getTimestamp(columnName).getTime());
    }

    @Override
    public DateTime getResult(ResultSet rs, int columnIndex) throws SQLException {
        return new DateTime(rs.getTimestamp(columnIndex).getTime());
    }

    @Override
    public DateTime getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return new DateTime(cs.getTimestamp(columnIndex).getTime());
    }
}
