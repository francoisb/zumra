package zumra.mappers;

import zumra.domain.ScreenName;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

@MappedTypes(ScreenName.class)
public class ScreenNameTypeHandler implements TypeHandler<ScreenName> {
    @Override
    public void setParameter(PreparedStatement ps, int i, ScreenName parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.VARCHAR);
        } else {
            ps.setString(i, parameter.getName());
        }
    }

    @Override
    public ScreenName getResult(ResultSet rs, String columnName) throws SQLException {
        return new ScreenName(rs.getString(columnName));
    }

    @Override
    public ScreenName getResult(ResultSet rs, int columnIndex) throws SQLException {
        return new ScreenName(rs.getString(columnIndex));
    }

    @Override
    public ScreenName getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return new ScreenName(cs.getString(columnIndex));
    }
}
