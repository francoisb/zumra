package zumra.mappers;

import zumra.domain.Media;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.*;

@MappedTypes(Media.class)
public class MediaTypeHandler implements TypeHandler<Media> {
    @Override
    public void setParameter(PreparedStatement ps, int i, Media parameter, JdbcType jdbcType) throws SQLException {
        if (parameter == null) {
            ps.setNull(i, Types.OTHER);
        } else {
            ps.setObject(i, parameter.getServiceName(), Types.OTHER);
        }
    }

    @Override
    public Media getResult(ResultSet rs, String columnName) throws SQLException {
        return Media.valueOf(rs.getString(columnName));
    }

    @Override
    public Media getResult(ResultSet rs, int columnIndex) throws SQLException {
        return Media.valueOf(rs.getString(columnIndex));
    }

    @Override
    public Media getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return Media.valueOf(cs.getString(columnIndex));
    }
}
