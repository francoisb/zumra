package zumra.mappers;

import com.sun.istack.NotNull;

public class Version {
    @NotNull
    private String version;

    Version() {
    }

    public Version(String version) {
        if (version == null) throw new IllegalArgumentException("version cannot be null");
        this.version = version;
    }

    /* This is private because it's only used by MyBatis. Regular users don't need access to this. */
    private void setVersion(String version) {
        if (version == null) throw new IllegalArgumentException("version cannot be null");
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Version version1 = (Version) o;

        return version.equals(version1.version);
    }

    @Override
    public int hashCode() {
        return version.hashCode();
    }

    @Override
    public String toString() {
        return "Version{" +
                "version='" + version + '\'' +
                '}';
    }
}
