package zumra.utils;

import zumra.domain.Hostname;
import zumra.domain.Link;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CanonicalizeUrl {
    private static final Logger LOG = LoggerFactory.getLogger(CanonicalizeUrl.class);
    private final CloseableHttpClient httpclient;

    public CanonicalizeUrl() {
        RequestConfig conf = RequestConfig.custom()
                .setAuthenticationEnabled(false)
                .setCircularRedirectsAllowed(false)
                .setConnectionRequestTimeout((int) TimeUnit.SECONDS.toMillis(15))
                .setConnectTimeout((int) TimeUnit.SECONDS.toMillis(15))
                .setMaxRedirects(21)
                .setRedirectsEnabled(true)
                .setRelativeRedirectsAllowed(true)
                .setSocketTimeout((int) TimeUnit.SECONDS.toMillis(15))
                .build();

        this.httpclient = HttpClients
                .custom()
                .setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36") // Copied from http://useragentstring.com/pages/Chrome/
                .setDefaultRequestConfig(conf)
                .build();
    }

    public Link canonicalize(Link link) throws IOException, URISyntaxException {
        URI uri = link.getUrl();

        LOG.debug("Canonicalizing [{}]", uri);
        HttpClientContext context = HttpClientContext.create();
        HttpHead httpget = new HttpHead(uri);
        try (CloseableHttpResponse response = httpclient.execute(httpget, context)) {
            HttpHost target = context.getTargetHost();
            List<URI> redirectLocations = context.getRedirectLocations();
            URI location = URIUtils.resolve(httpget.getURI(), target, redirectLocations);

            final URI targetUri = new URI(location.toASCIIString());
            LOG.info("Canonicalized [{}] to [{}]", uri, targetUri);

            return new Link(
                    targetUri,
                    new Hostname(targetUri.getHost()),
                    link.getService(),
                    link.getBody(),
                    link.getServiceId(),
                    link.getUserId(),
                    link.getScreenName(),
                    link.getCreatedAt(),
                    link.getHarvestedAt());
        } catch (SSLHandshakeException e) {
            LOG.warn("Failed SSL handshake for {}: {}; returning original URL", uri, e.getMessage());
            return link;
        } catch (ClientProtocolException e) {
            LOG.warn("Failed to canonicalize {}: {}; returning original URL", uri, e.getMessage());
            return link;
        }
    }
}
