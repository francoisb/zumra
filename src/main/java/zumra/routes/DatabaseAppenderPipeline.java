package zumra.routes;

import zumra.camel.LinkListAggregator;
import zumra.utils.CanonicalizeUrl;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.ibatis.session.SqlSessionFactory;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

public class DatabaseAppenderPipeline extends RouteBuilder {
    private final String initialEndpoint;
    private final SqlSessionFactory sqlSessionFactory;

    public DatabaseAppenderPipeline(String initialEndpoint, SqlSessionFactory sqlSessionFactory) {
        super();
        this.initialEndpoint = initialEndpoint;
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public void configure() throws Exception {
        final UpdateLinksTable worker = new UpdateLinksTable(sqlSessionFactory);

        errorHandler(defaultErrorHandler()
                .backOffMultiplier(2.0) // exponential backoff
                .logStackTrace(false) // don't bother with huge backtraces when the problem is an external network error
                .collisionAvoidancePercent(0.15) // add some jitter between attempts, so that all attempts don't fall at the same time
                .asyncDelayedRedelivery() // do not tie up a thread while retrying
        );

        onException(SQLException.class)
                .redeliveryDelay(TimeUnit.SECONDS.toMillis(3))
                .maximumRedeliveryDelay(TimeUnit.MINUTES.toMillis(2)) // but wait at most 2 minutes between attempts
                .maximumRedeliveries(5) // try up to 5 times
                .retryAttemptedLogLevel(LoggingLevel.INFO)
                .retriesExhaustedLogLevel(LoggingLevel.WARN);

        onException(Exception.class)
                .redeliveryDelay(TimeUnit.SECONDS.toMillis(7))
                .maximumRedeliveryDelay(TimeUnit.MINUTES.toMillis(30)) // but wait at most 30 minutes between attempts
                .maximumRedeliveries(30) // try up to 30 times
                .retryAttemptedLogLevel(LoggingLevel.DEBUG)
                .retriesExhaustedLogLevel(LoggingLevel.INFO);

        from(initialEndpoint)
                .routeId("database-appender")
                .filter(simple("${body.validServiceId}"))
                .bean(CanonicalizeUrl.class)
                .aggregate().expression(constant(true)).completionInterval(TimeUnit.MINUTES.toMillis(1)).completionSize(100).forceCompletionOnStop().parallelProcessing().aggregationStrategy(new LinkListAggregator())
                .log(LoggingLevel.DEBUG, "Want to write ${body.size} items to the database")
                .bean(worker, "insertLinks")
        ;
    }
}
