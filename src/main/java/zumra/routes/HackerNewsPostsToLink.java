package zumra.routes;

import zumra.domain.*;
import org.apache.camel.language.XPath;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * <Page xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 * <items>
 * <PageItem>
 * <title>Kingston and PNY using cheaper components after good reviews</title>
 * <url>http://www.extremetech.com/extreme/184253-ssd-shadiness-kingston-and-pny-caught-bait-and-switching-cheaper-components-after-good-reviews</url>
 * <id>7893068</id>
 * <postedAgo>3 hours ago</postedAgo>
 * <postedBy>nkurz</postedBy>
 * </PageItem>
 * <PageItem>
 * <title>Marvel Comics and the “most intricate fictional narrative in history” (2013)</title>
 * <url>http://theappendix.net/issues/2012/12/days-of-future-present-marvel-comics-and-the-most-intricate-fictional-narrative-in-the-history-of-the-world</url>
 * <id>7893468</id>
 * <commentCount>0</commentCount>
 * <points>13</points>
 * <postedAgo>34 minutes ago</postedAgo>
 * <postedBy>drjohnson</postedBy>
 * </PageItem>
 */
public class HackerNewsPostsToLink {
    public static Link convert(@XPath("//title") final String title,
                               @XPath("//url") final String url,
                               @XPath("//id") final long id,
                               @XPath("//postedAgo") final String postedAgo,
                               @XPath("//postedBy") final String postedBy) throws URISyntaxException {
        final MutableDateTime postedAt = new MutableDateTime();
        if (!postedAgo.isEmpty()) {
            final int scalar = -1 * Integer.parseInt(postedAgo.substring(0, postedAgo.indexOf(" ")));
            if (postedAgo.indexOf("days") > 0) postedAt.addDays(scalar);
            if (postedAgo.indexOf("hours") > 0) postedAt.addHours(scalar);
            if (postedAgo.indexOf("minutes") > 0) postedAt.addMinutes(scalar);
            if (postedAgo.indexOf("seconds") > 0) postedAt.addSeconds(scalar);
        }

        final URI baseUri = new URI(url.trim());
        final URI finalUri;
        if (baseUri.getHost() == null || baseUri.getHost().isEmpty()) {
            finalUri = new URI("https://news.ycombinator.com" + url.trim());
        } else {
            finalUri = baseUri;
        }

        return new Link(
                finalUri,
                new Hostname(finalUri.getHost()),
                Media.HACKER_NEWS,
                title,
                new ItemId(id),
                null,
                new ScreenName(postedBy),
                postedAt.toDateTime(),
                new DateTime()
        );
    }
}
