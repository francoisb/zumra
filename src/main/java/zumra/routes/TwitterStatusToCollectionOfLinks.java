package zumra.routes;

import zumra.domain.*;
import org.joda.time.DateTime;
import twitter4j.Status;
import twitter4j.URLEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

public class TwitterStatusToCollectionOfLinks {
    public static Set<Link> convert(Status status) throws URISyntaxException {
        final Set<Link> converted = new HashSet<>(status.getURLEntities().length);
        for (URLEntity entity : status.getURLEntities()) {
            final URI uri = new URI(entity.getExpandedURL());
            final Link link = new Link(
                    uri,
                    new Hostname(uri.getHost()),
                    Media.TWITTER,
                    status.getText(),
                    new ItemId(status.getId()),
                    new UserId(status.getUser().getId()),
                    new ScreenName(status.getUser().getScreenName()),
                    new DateTime(status.getCreatedAt()),
                    new DateTime());

            converted.add(link);
        }

        return converted;
    }
}
