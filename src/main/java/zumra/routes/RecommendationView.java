package zumra.routes;

import org.apache.camel.Exchange;

import javax.servlet.http.HttpServletResponse;

public class RecommendationView {
    public void transform(final Exchange exchange) {
        final HttpServletResponse in = exchange.getIn(HttpServletResponse.class);
        in.setHeader("Content-Type", "text/html;charset=utf-8");
        in.setHeader("Cache-Control", "no-cache");
    }
}
