package zumra.routes;

import org.apache.camel.Exchange;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectToHome {
    public void redirectTo(final Exchange exchange) throws IOException {
        final HttpServletResponse out = exchange.getIn(HttpServletResponse.class);
        out.sendRedirect("/");
    }
}
