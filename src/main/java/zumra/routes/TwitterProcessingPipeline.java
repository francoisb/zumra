package zumra.routes;

import zumra.harvesters.TwitterBackfillHarvester;
import org.apache.camel.builder.RouteBuilder;
import twitter4j.Status;

import java.util.concurrent.TimeUnit;

public class TwitterProcessingPipeline extends RouteBuilder {
    private final String initialEndpoint;
    private final String targetEndpoint;
    private final TwitterBackfillHarvester backfillHarvester;

    public TwitterProcessingPipeline(String initialEndpoint, String targetEndpoint, TwitterBackfillHarvester twitterBackfillHarvester) {
        super();
        this.initialEndpoint = initialEndpoint;
        this.targetEndpoint = targetEndpoint;
        this.backfillHarvester = twitterBackfillHarvester;
    }

    @Override
    public void configure() throws Exception {
        from("timer:twitter-backfill-home-timeline?repeatCount=1&delay=" + TimeUnit.SECONDS.toMillis(11))
                .routeId("twitter-backfill-home-timeline")
                .bean(backfillHarvester, "backfill")
                .to(initialEndpoint);

        from(initialEndpoint)
                .routeId("twitter-interaction-pipeline")
                .filter().method(this, "hasAnyUrls")
                .bean(TwitterStatusToCollectionOfLinks.class)
                .split(body())
                .to(targetEndpoint);
    }

    public boolean hasAnyUrls(final Status status) {
        return status != null && status.getURLEntities() != null && status.getURLEntities().length > 0;
    }
}
