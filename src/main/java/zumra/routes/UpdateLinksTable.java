package zumra.routes;

import zumra.domain.Link;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class UpdateLinksTable {
    private static final Logger LOG = LoggerFactory.getLogger(UpdateLinksTable.class);

    private final SqlSessionFactory sqlSessionFactory;

    public UpdateLinksTable(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public void insertLinks(Collection<Link> links) throws SQLException {
        try (final SqlSession session = sqlSessionFactory.openSession(false)) {
            final Connection conn = session.getConnection();

            LOG.debug("Creating temporary table");
            try (final Statement stat = conn.createStatement()) {
                stat.execute("CREATE TEMPORARY TABLE tmp_links(url text, hostname text, service media, service_id bigint, user_id bigint, screen_name text, harvested_at timestamp with time zone, body text) ON COMMIT DROP");
            }

            LOG.debug("Inserting raw link data to temporary table ({} rows)", links.size());
            final int insertedRawLinks = session.insert("prepareLinks", links);

            LOG.debug("Inserting missing links to links table");
            final int insertedLinks = session.insert("insertLinks");

            LOG.debug("Inserting missing sources to link_sources tables");
            final int insertedSources = session.insert("insertLinkSources");

            LOG.info("Database updated: {} raw links, {} new links, {} new sources", insertedRawLinks, insertedLinks, insertedSources);

            session.commit();
        } catch (SQLException e) {
            LOG.warn("Failed to write {} links to the database", links.size(), e);
            throw e;
        }
    }
}
