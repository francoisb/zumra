package zumra.routes;

import zumra.ui.ExtractListOfIdsFromRequest;
import zumra.ui.HttpGo;
import org.apache.camel.builder.RouteBuilder;

import static java.lang.String.format;

public class ClientUI extends RouteBuilder {
    private final String host;
    private final int port;
    private final boolean enableTemplateCaching;

    public ClientUI(String host, int port, boolean enableTemplateCaching) {
        this.host = host;
        this.port = port;
        this.enableTemplateCaching = enableTemplateCaching;
    }

    @Override
    public void configure() throws Exception {
        final String baseUrl = format("jetty:http://%s:%d", host, port);

        from(baseUrl + "/")
                .routeId("ui-list-recommendations")
                .to("mybatis:findBestRecommendations?statementType=SelectList")
                .log("Recommendations: ${body}")
                .to("velocity://info/teksol/templates/findBestRecommendations.html.vm?encoding=utf-8&contentCache=" + String.valueOf(enableTemplateCaching))
                .removeHeader("breadcrumbId")
                .removeHeader("User-Agent")
                .removeHeader("Accept")
                .removeHeader("Accept-Encoding")
                .removeHeader("Accept-Language")
                .removeHeader("Cookie")
                .removeHeader("Referer")
                .removeHeader("DNT")
                .bean(RecommendationView.class)
        ;

        from(baseUrl + "/discard")
                .routeId("ui-discard")
                .bean(ExtractListOfIdsFromRequest.class)
                .log("Discarding links with ids:${body.ids}")
                .to("mybatis:discardLinks?statementType=Update")
                .bean(RedirectToHome.class)
        ;

        from(baseUrl + "/go")
                .routeId("ui-go")
                .bean(ExtractListOfIdsFromRequest.class)
                .to("mybatis:tagLinkAsInteresting?statementType=Update")
                .to("mybatis:findUrlById?statementType=SelectOne")
                .bean(HttpGo.class)
        ;
    }
}
