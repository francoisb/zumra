package zumra.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.w3c.dom.Document;

import java.util.concurrent.TimeUnit;

public class HackerNewsProcessingPipeline extends RouteBuilder {
    private final String targetEndpoint;

    public HackerNewsProcessingPipeline(String targetEndpoint) {
        super();
        this.targetEndpoint = targetEndpoint;
    }

    @Override
    public void configure() throws Exception {
        from("timer:hackernews?period=" + TimeUnit.MINUTES.toMillis(11) + "&delay=" + TimeUnit.SECONDS.toMillis(7))
                .routeId("hackernews-pipeline-fetcher")
                .to("http://api.ihackernews.com/page?format=xml").errorHandler(defaultErrorHandler().maximumRedeliveries(3).maximumRedeliveryDelay(TimeUnit.SECONDS.toMillis(30)).asyncDelayedRedelivery())
                .convertBodyTo(String.class)
                .choice()
                    .when(header(Exchange.HTTP_RESPONSE_CODE).isEqualTo(200)).to("direct:hackernews-interaction-pipeline-success")
                    .otherwise().log(LoggingLevel.WARN, "Failed to retrieve posts from HackerNews API: ${body}")
                .endChoice();

        from("direct:hackernews-interaction-pipeline-success")
                .routeId("hackernews-pipeline-persister")
                .convertBodyTo(Document.class)
                .split(xpath("//PageItem"))
                .bean(HackerNewsPostsToLink.class)
                .to(targetEndpoint);
    }
}
