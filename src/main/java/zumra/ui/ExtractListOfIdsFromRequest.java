package zumra.ui;

import zumra.domain.Votes;
import org.apache.camel.Exchange;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ExtractListOfIdsFromRequest {
    public Votes convert(final Exchange exchange) throws URISyntaxException {
        final List<String> rawIds = extractRawIds(exchange);
        final Set<Integer> ids = rawIds.stream().map(Integer::parseInt).collect(Collectors.toSet());

        return new Votes(ids);
    }

    @SuppressWarnings("unchecked")
    private List<String> extractRawIds(Exchange exchange) {
        final List<String> rawIds;
        if (exchange.getIn().getHeader("id", List.class) == null) {
            rawIds = Collections.singletonList(exchange.getIn().getHeader("id", String.class));
        } else {
            rawIds = exchange.getIn().getHeader("id", List.class);
        }

        return rawIds;
    }
}
