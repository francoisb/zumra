package zumra.ui;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;

public class HttpGo {
    private static final Logger LOG = LoggerFactory.getLogger(HttpGo.class);

    public void go(final URI target, final Exchange exchange) throws URISyntaxException {
        if (target == null) {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 400);
            exchange.getOut().setBody("<html><head><title>Failed!</title><body><h1>Failed to redirect properly</h1><p>You attempted to vote for a URL, but the URL did not exist in the database anymore.</p><p>You might ignore this if the database is in flux, but if it's not, this is a bug that should be reported.</p></html>");
        } else {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 303 /* See Other */);
            exchange.getOut().setHeader("Location", target);
        }
    }
}
