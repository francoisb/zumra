package zumra.camel;

import zumra.domain.Link;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AbstractListAggregationStrategy;

public class LinkListAggregator extends AbstractListAggregationStrategy<Link> {
    @Override
    public Link getValue(Exchange exchange) {
        return (Link) exchange.getIn().getBody();
    }
}
