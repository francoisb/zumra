package zumra.camel;

import zumra.harvesters.TwitterStreamingHarvester;
import org.apache.camel.CamelContext;
import org.apache.camel.StartupListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BootTwitterConsumersOnceContextStarts implements StartupListener {
    private static final Logger LOG = LoggerFactory.getLogger(BootTwitterConsumersOnceContextStarts.class);
    private final TwitterStreamingHarvester streamingHarvester;

    public BootTwitterConsumersOnceContextStarts(TwitterStreamingHarvester streamingHarvester) {
        this.streamingHarvester = streamingHarvester;
    }

    @Override
    public void onCamelContextStarted(CamelContext context, boolean alreadyStarted) throws Exception {
        LOG.info("Booting Twitter streaming harvester");
        streamingHarvester.start();
    }
}
