package zumra;

import java.net.URI;

public class Doc {
    public long id;
    public URI uri;
    public String hostname;
    public String service;
    public String screenName;
    public String body;
    public boolean vote;
}
