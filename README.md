# Zumra

An experiment to see if I can find the most interesting articles (for myself) from amongst
Hacker News and Twitter. In order to do that, I poll Hacker News every few minutes, and stream
Twitter. Whenever a new link is found, I store it to the database. Every hour, the classifier
kicks in and builds recommendations. The UI allows the reader to classify links that are
interesting vs others that should simply be discarded.

# Running

1. Create a [new Twitter application](https://apps.twitter.com/app/new).
2. Get [VirtualBox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/)
3. Run:

    $ vagrant up
    $ vagrant ssh
    $ cd /vagrant
    $ sudo su -
    # cpan App::Sqitch DBD::Pg
    # exit
    $ bin/resetdb
    $ mkdir .env
    $ echo > .env/DATABASE_URL "jdbc:postgresql://USER:PASS@HOST:PORT/DB"
    $ echo > .env/TWITTER_ACCESS_TOKEN ""
    $ echo > .env/TWITTER_ACCESS_TOKEN_SECRET ""
    $ echo > .env/TWITTER_CONSUMER_KEY ""
    $ echo > .env/TWITTER_CONSUMER_SECRET ""
    $ echo > .env/TWITTER_STREAM_ENABLED "true"
    $ echo > .env/PORT "8093"
    $ bin/run

The last step compiles and runs the system.

# Implementation

This project is also a learning experience regarding [Apache Camel](http://camel.apache.org/).
Before this project, I kind of knew what Camel was, but had never used it. After having played
quite a bit with Camel, I've found many things to like.

# License

Copyright © 2014, François Beausoleil <francois@teksol.info>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
