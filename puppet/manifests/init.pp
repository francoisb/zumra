package{[
  "git",
  "htop",
  "vim-nox",
  "byobu",
  "zsh",
  "curl",
  "build-essential",
  "postgresql-9.3",
  "postgresql-client-9.3",
  "postgresql-contrib-9.3",
  "postgresql-server-dev-9.3",
  "libpq-dev",
  "maven",
  "python-software-properties",
  "daemontools",
  "ruby1.9.1-full",
]:
  ensure => latest,
}

exec{'install-java8-repository':
  command => '/usr/bin/add-apt-repository -y ppa:webupd8team/java',
  creates => '/etc/apt/sources.list.d/webupd8team-java-trusty.list',
  before  => Exec['/usr/bin/apt-get update'],
}

exec{'accept-oracle-license':
  command => '/bin/echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections',
  unless  => '/bin/grep --quiet oracle-java8-installer /var/cache/debconf/config.dat',
  require => Exec['install-java8-repository'],
}

package{'oracle-java8-installer':
  require => Exec['accept-oracle-license'],
}

package{'oracle-java8-set-default':
  require => Package['oracle-java8-installer'],
}

$puppet_package_path = '/usr/local/src/puppetlabs-trusty.deb'
exec{'download-puppet':
  command => "/usr/bin/wget -O ${puppet_package_path} https://apt.puppetlabs.com/puppetlabs-release-trusty.deb",
  creates => $puppet_package_path,
}

exec{'install-puppet-repository':
  command => "/usr/bin/dpkg -i $puppet_package_path",
  require => Exec['download-puppet'],
  before  => Exec['/usr/bin/apt-get update'],
}

exec{'install-sqitch':
  command => '/usr/bin/cpan -i -T App::Sqitch DBD::Pg',
  timeout => 600,
  require => Exec['/usr/bin/apt-get upgrade -y'],
}

# Global actions, pre-anything else
exec{'/usr/bin/apt-get update': }
exec{'/usr/bin/apt-get upgrade -y': }

Exec['/usr/bin/apt-get update'] -> Exec['/usr/bin/apt-get upgrade -y']
Exec['/usr/bin/apt-get upgrade -y'] -> Package <| |>
