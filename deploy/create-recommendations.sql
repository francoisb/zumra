-- Deploy create-recommendations
-- requires: create-links
SET client_min_messages TO 'warning';

BEGIN;

  CREATE VIEW recommendations AS
      SELECT id, url, hostname, weight, array_agg(body) bodies, min(harvested_at) harvested_at
      FROM link_weights
        JOIN link_sources USING (url)
        JOIN links USING (url)
      WHERE voted_at IS NULL
      GROUP BY id, url, hostname, weight
    UNION ALL
      SELECT id, url, hostname, -1.0 weight, array_agg(body) bodies, min(harvested_at) harvested_at
      FROM links
        JOIN link_sources USING (url)
      WHERE voted_at IS NULL
      GROUP BY id, url, hostname, weight
    ORDER BY weight DESC, harvested_at DESC
    LIMIT 50;

COMMIT;
