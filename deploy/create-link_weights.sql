-- Deploy create-link_weights
-- requires: create-links
SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE link_weights(
      url text primary key references links
    , weight double precision not null check(weight between 0.0 and 1.0)
  );

COMMIT;
