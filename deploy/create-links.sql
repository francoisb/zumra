-- Deploy create-links
-- requires: create-custom-types
-- requires: create-users
SET client_min_messages TO 'warning';

BEGIN;

  CREATE TABLE links(
      url text not null
    , id serial not null unique
    , hostname text not null
    , created_at timestamp with time zone not null
    , vote boolean
    , voted_at timestamp with time zone

    , constraint vote_and_voted_at_are_consistent check((vote IS NULL AND voted_at IS NULL) OR (vote IS NOT NULL AND voted_at IS NOT NULL))

    , primary key(url)
  );

  CREATE TABLE link_sources(
      url text not null references links

    , service media not null
    , service_id bigint not null check(service_id > 0)
    , user_id bigint
    , screen_name text not null
    , harvested_at timestamptz not null
    , created_at timestamptz not null
    , body text not null

    , constraint user_id_must_be_set_unless_hackernews check((service = 'Hacker News' AND user_id IS NULL) OR (user_id IS NOT NULL))

    , primary key(url, service, service_id)
  );

  CREATE FUNCTION maintain_voted_at() RETURNS TRIGGER AS $$
  BEGIN
    IF NEW.vote IS NULL THEN
      NEW.voted_at := NULL;
    ELSE
      NEW.voted_at := now();
    END IF;

    RETURN NEW;
  END
  $$ LANGUAGE 'plpgsql';

  CREATE TRIGGER maintain_links_voted_at BEFORE UPDATE OF vote
  ON links
  FOR EACH ROW EXECUTE PROCEDURE maintain_voted_at();

  CREATE TRIGGER maintain_links_created_at BEFORE INSERT
  ON links
  FOR EACH ROW EXECUTE PROCEDURE maintain_created_at();

  CREATE TRIGGER maintain_link_sources_created_at BEFORE INSERT
  ON link_sources
  FOR EACH ROW EXECUTE PROCEDURE maintain_created_at();

COMMIT;
