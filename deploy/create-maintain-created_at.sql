-- Deploy create-maintain-created_at
SET client_min_messages TO 'warning';

BEGIN;

  CREATE FUNCTION maintain_created_at() RETURNS TRIGGER AS $$
  BEGIN
    NEW.created_at := transaction_timestamp();
    RETURN NEW;
  END
  $$ LANGUAGE 'plpgsql' SECURITY DEFINER;

COMMIT;
