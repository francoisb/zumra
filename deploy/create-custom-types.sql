-- Deploy create-custom-types
SET client_min_messages TO 'warning';

BEGIN;

  CREATE TYPE media AS ENUM (
    'Facebook',
    'Twitter',
    'Google+',
    'Hacker News'
  );

COMMIT;
