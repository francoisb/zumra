-- Verify create-links

BEGIN;

  SELECT url, hostname
  FROM links
  WHERE 1 = 0;

ROLLBACK;
