-- Revert create-custom-types
SET client_min_messages TO 'warning';

BEGIN;

  DROP TYPE media CASCADE;

COMMIT;
