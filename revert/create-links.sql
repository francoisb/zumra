-- Revert create-links
SET client_min_messages TO 'warning';

BEGIN;

  DROP TABLE links CASCADE;
  DROP TABLE link_sources CASCADE;
  DROP FUNCTION maintain_voted_at();

COMMIT;
