-- Revert create-maintain-created_at
SET client_min_messages TO 'warning';
SET SESSION AUTHORIZATION hnapi;

BEGIN;

  DROP FUNCTION maintain_created_at();

COMMIT;
