-- Revert create-recommendations
SET client_min_messages TO 'warning';
SET SESSION AUTHORIZATION hnapi;

BEGIN;

  DROP VIEW recommendations;

COMMIT;
