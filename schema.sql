--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE hnapifb;
ALTER ROLE hnapifb WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE hnapigp;
ALTER ROLE hnapigp WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE hnapiharvester;
ALTER ROLE hnapiharvester WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE hnapihn;
ALTER ROLE hnapihn WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE hnapimig;
ALTER ROLE hnapimig WITH SUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE hnapirecommender;
ALTER ROLE hnapirecommender WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE hnapitw;
ALTER ROLE hnapitw WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE hnapiweb;
ALTER ROLE hnapiweb WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION;


--
-- Role memberships
--

GRANT hnapiharvester TO hnapifb GRANTED BY postgres;
GRANT hnapiharvester TO hnapigp GRANTED BY postgres;
GRANT hnapiharvester TO hnapihn GRANTED BY postgres;
GRANT hnapiharvester TO hnapitw GRANTED BY postgres;




--
-- Database creation
--

CREATE DATABASE hnapi WITH TEMPLATE = template0 OWNER = hnapimig ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
REVOKE ALL ON DATABASE hnapi FROM PUBLIC;
REVOKE ALL ON DATABASE hnapi FROM hnapimig;
GRANT ALL ON DATABASE hnapi TO hnapimig;
GRANT TEMPORARY ON DATABASE hnapi TO PUBLIC;
GRANT CONNECT ON DATABASE hnapi TO hnapiharvester;
GRANT CONNECT ON DATABASE hnapi TO hnapiweb;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect hnapi

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: service; Type: TYPE; Schema: public; Owner: hnapimig
--

CREATE TYPE service AS ENUM (
    'Facebook',
    'Twitter',
    'Google+',
    'Hacker News'
);


ALTER TYPE public.service OWNER TO hnapimig;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: discards; Type: TABLE; Schema: public; Owner: hnapimig; Tablespace: 
--

CREATE TABLE discards (
    post_id integer NOT NULL,
    tagged_at timestamp with time zone NOT NULL
);


ALTER TABLE public.discards OWNER TO hnapimig;

--
-- Name: plus_ones; Type: TABLE; Schema: public; Owner: hnapimig; Tablespace: 
--

CREATE TABLE plus_ones (
    post_id integer NOT NULL,
    tagged_at timestamp with time zone NOT NULL
);


ALTER TABLE public.plus_ones OWNER TO hnapimig;

--
-- Name: raw_posts; Type: TABLE; Schema: public; Owner: hnapimig; Tablespace: 
--

CREATE TABLE raw_posts (
    post_id integer NOT NULL,
    service service NOT NULL,
    service_id bigint NOT NULL,
    from_screen_name text NOT NULL,
    title text NOT NULL,
    hostname text NOT NULL,
    posted_at timestamp with time zone NOT NULL,
    retweeted boolean NOT NULL,
    url text NOT NULL
);


ALTER TABLE public.raw_posts OWNER TO hnapimig;

--
-- Name: posts_post_id_seq; Type: SEQUENCE; Schema: public; Owner: hnapimig
--

CREATE SEQUENCE posts_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_post_id_seq OWNER TO hnapimig;

--
-- Name: posts_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hnapimig
--

ALTER SEQUENCE posts_post_id_seq OWNED BY raw_posts.post_id;


--
-- Name: relevant_posts; Type: TABLE; Schema: public; Owner: hnapimig; Tablespace: 
--

CREATE TABLE relevant_posts (
    post_id integer NOT NULL,
    tagged_at timestamp with time zone NOT NULL
);


ALTER TABLE public.relevant_posts OWNER TO hnapimig;

--
-- Name: post_id; Type: DEFAULT; Schema: public; Owner: hnapimig
--

ALTER TABLE ONLY raw_posts ALTER COLUMN post_id SET DEFAULT nextval('posts_post_id_seq'::regclass);


--
-- Data for Name: discards; Type: TABLE DATA; Schema: public; Owner: hnapimig
--

COPY discards (post_id, tagged_at) FROM stdin;
\.


--
-- Data for Name: plus_ones; Type: TABLE DATA; Schema: public; Owner: hnapimig
--

COPY plus_ones (post_id, tagged_at) FROM stdin;
\.


--
-- Name: posts_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hnapimig
--

SELECT pg_catalog.setval('posts_post_id_seq', 1, false);


--
-- Data for Name: raw_posts; Type: TABLE DATA; Schema: public; Owner: hnapimig
--

COPY raw_posts (post_id, service, service_id, from_screen_name, title, hostname, posted_at, retweeted, url) FROM stdin;
\.


--
-- Data for Name: relevant_posts; Type: TABLE DATA; Schema: public; Owner: hnapimig
--

COPY relevant_posts (post_id, tagged_at) FROM stdin;
\.


--
-- Name: discards_pkey; Type: CONSTRAINT; Schema: public; Owner: hnapimig; Tablespace: 
--

ALTER TABLE ONLY discards
    ADD CONSTRAINT discards_pkey PRIMARY KEY (post_id);


--
-- Name: plus_ones_pkey; Type: CONSTRAINT; Schema: public; Owner: hnapimig; Tablespace: 
--

ALTER TABLE ONLY plus_ones
    ADD CONSTRAINT plus_ones_pkey PRIMARY KEY (post_id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: hnapimig; Tablespace: 
--

ALTER TABLE ONLY raw_posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (service_id, service);


--
-- Name: raw_posts_post_id_key; Type: CONSTRAINT; Schema: public; Owner: hnapimig; Tablespace: 
--

ALTER TABLE ONLY raw_posts
    ADD CONSTRAINT raw_posts_post_id_key UNIQUE (post_id);


--
-- Name: relevant_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: hnapimig; Tablespace: 
--

ALTER TABLE ONLY relevant_posts
    ADD CONSTRAINT relevant_posts_pkey PRIMARY KEY (post_id);


--
-- Name: discards_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnapimig
--

ALTER TABLE ONLY discards
    ADD CONSTRAINT discards_post_id_fkey FOREIGN KEY (post_id) REFERENCES raw_posts(post_id);


--
-- Name: plus_ones_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnapimig
--

ALTER TABLE ONLY plus_ones
    ADD CONSTRAINT plus_ones_post_id_fkey FOREIGN KEY (post_id) REFERENCES raw_posts(post_id);


--
-- Name: relevant_posts_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hnapimig
--

ALTER TABLE ONLY relevant_posts
    ADD CONSTRAINT relevant_posts_post_id_fkey FOREIGN KEY (post_id) REFERENCES raw_posts(post_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: discards; Type: ACL; Schema: public; Owner: hnapimig
--

REVOKE ALL ON TABLE discards FROM PUBLIC;
REVOKE ALL ON TABLE discards FROM hnapimig;
GRANT ALL ON TABLE discards TO hnapimig;
GRANT INSERT ON TABLE discards TO hnapiweb;


--
-- Name: plus_ones; Type: ACL; Schema: public; Owner: hnapimig
--

REVOKE ALL ON TABLE plus_ones FROM PUBLIC;
REVOKE ALL ON TABLE plus_ones FROM hnapimig;
GRANT ALL ON TABLE plus_ones TO hnapimig;
GRANT INSERT ON TABLE plus_ones TO hnapiweb;


--
-- Name: raw_posts; Type: ACL; Schema: public; Owner: hnapimig
--

REVOKE ALL ON TABLE raw_posts FROM PUBLIC;
REVOKE ALL ON TABLE raw_posts FROM hnapimig;
GRANT ALL ON TABLE raw_posts TO hnapimig;
GRANT INSERT ON TABLE raw_posts TO hnapiharvester;
GRANT SELECT ON TABLE raw_posts TO hnapiweb;


--
-- Name: relevant_posts; Type: ACL; Schema: public; Owner: hnapimig
--

REVOKE ALL ON TABLE relevant_posts FROM PUBLIC;
REVOKE ALL ON TABLE relevant_posts FROM hnapimig;
GRANT ALL ON TABLE relevant_posts TO hnapimig;
GRANT INSERT,DELETE ON TABLE relevant_posts TO hnapirecommender;
GRANT SELECT ON TABLE relevant_posts TO hnapiweb;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

